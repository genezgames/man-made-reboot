﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class player : MonoBehaviour {

	public AudioClip[] steps;
	public AudioClip jump;
	public AudioClip push;
	public AudioClip brief;
	public AudioClip soultaken;
	public AudioClip[] hurt;
	public float positionx;
	public float positionz;
	public float positiony;
	public float stepspeed;
	public GameObject PlayerChara;
	public GameObject PlayerCharb;

	public Image life;
	public Text maintext;
	public Text txt_soulspheres;
	public Text txt_colisor;

	public int numero_soulspheres; 

	public int confere_mao;
	public int confere_mao_passo1;
	public float confere_mao_tempo;
	public GameObject mao_esquerda;
	public GameObject mao_direita;

	public float r;
	public float g;
	public float b;
	public float a;

	public float playertoghost;


	// Use this for initialization
	void Start () {
		positionx = transform.position.x;
		positionz = transform.position.z;
		positiony = transform.position.y;

		confere_mao = 1;
		confere_mao_passo1 = 0;
		confere_mao_tempo = 0;

		numero_soulspheres = 100;

		life.rectTransform.sizeDelta = new Vector2 (PlayerChara.gameObject.GetComponent<Renderer> ().material.color.a * 1000.0f, 100.0f);

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Jump")) {
			GetComponent<AudioSource> ().PlayOneShot (jump);
		}
		if ((positionx != transform.position.x || positionz != transform.position.z) && (positiony == transform.position.y)) {
			stepspeed += Time.deltaTime;
			if (stepspeed > Random.Range(0.2f, 0.3f)){
				GetComponent<AudioSource> ().PlayOneShot (steps [Random.Range (0, 5)]);
				stepspeed = 0;
			}
		}
		positionx = transform.position.x;
		positionz = transform.position.z;
		positiony = transform.position.y;

		playertoghost += Time.deltaTime;

		if (playertoghost >= 5 && numero_soulspheres > 0) {
			
			GetComponent<AudioSource> ().PlayOneShot (hurt [Random.Range (0, 3)]);

			r = PlayerChara.gameObject.GetComponent<Renderer> ().material.color.r;
			g = PlayerChara.gameObject.GetComponent<Renderer> ().material.color.g;
			b = PlayerChara.gameObject.GetComponent<Renderer> ().material.color.b;
			a = PlayerChara.gameObject.GetComponent<Renderer> ().material.color.a - 0.01f;

			PlayerChara.gameObject.GetComponent<Renderer> ().material.color = new Color(r,g,b,a);
			PlayerCharb.gameObject.GetComponent<Renderer> ().material.color = new Color(r,g,b,a);

			GetComponent<AudioSource> ().pitch = a;

			playertoghost = 0.0f;

			confere_mao = Random.Range(0,2);

			if (a <= 0) {
				maintext.text = "Game Over";
				gameObject.GetComponent<CapsuleCollider> ().enabled = false;
			}
		}

		life.rectTransform.sizeDelta = new Vector2 (a * 1000.0f, 100.0f);
		numero_soulspheres = GameObject.FindGameObjectsWithTag ("soulsphere").Length;
		txt_soulspheres.text = numero_soulspheres.ToString();

		if (numero_soulspheres <= 0) {
			maintext.text = "Congratulations - Now you are Ready";
		}

		if (confere_mao == 1) {
			confere_mao_tempo += Time.deltaTime;
			if (confere_mao_tempo > 0.1f) {
				if (mao_esquerda.transform.rotation.y > 0f && confere_mao_passo1 == 0) {
					//mao_esquerda.transform.eulerAngles = new Vector3 (0, mao_esquerda.transform.rotation.y - 0.01f, 0);
				} else {
					confere_mao_passo1 = 1;
				}
				if (mao_esquerda.transform.rotation.y < 160.0f && confere_mao_passo1 == 1) {
					//mao_esquerda.transform.eulerAngles = new Vector3 (0, mao_esquerda.transform.rotation.y + 0.01f, 0);
				} else {
					confere_mao_passo1 = 0;
					confere_mao = 0;
				}
				confere_mao_tempo = 0;
			}
				
		}
		if (confere_mao == 2) {
			
		}
	}

	void OnTriggerEnter (Collider other){
		if (other.gameObject.CompareTag("soulsphere")){
			Destroy(other.gameObject);
			PlayerChara.gameObject.GetComponent<Renderer> ().material.color = new Color(r,g,b,a + 0.05f);
			PlayerCharb.gameObject.GetComponent<Renderer> ().material.color = new Color(r,g,b,a + 0.05f);
			GetComponent<AudioSource> ().PlayOneShot (soultaken);
		}
		print (other.gameObject.name);

		if (other.gameObject.CompareTag("plataforma")){
			//gameObject.transform.parent = other.gameObject.transform;
			print ("connect");
		}

		txt_colisor.text = other.gameObject.name;
	}

	void OnTriggerExit(Collider other){
		if (other.gameObject.CompareTag("plataforma")){
			//gameObject.transform.parent = null;
			print ("disconnect");
		}
	}
}
