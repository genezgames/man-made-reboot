﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class gamerun : MonoBehaviour {

	public Transform sala;
	public Transform sala2;
	public int pecarandomica;
	public float x;
	public float y;
	public float z;
	public float velocidade;
	public GameObject[] peca_padrao_giro;
	public GameObject jogador;
	public float ciclo;
	public float correnteciclo;
	public float move_pecas;
	public float teste_angulo_atual;

	public GameObject poeira;
	public GameObject[] gerador_poeira;
	public GameObject soulsphere;
	public GameObject[] criasoulsphere;

	public GameObject Ambiente;
	public AudioClip[] Ambiente_sounds;

	public float angulo_atual;
	public float angulo_maximo;

	// Use this for initialization

	void Start () {
		y = 0;
		velocidade = 1f;
		correnteciclo = 0.00f;
		ciclo = 15.00f;
		move_pecas = 0;
		angulo_atual = 0;
		angulo_maximo = 90.0f;

		for(x=1;x<10;x++){
			for(z=1;z<10;z++){
				if ((x > 3 && x < 7 && z > 3 && z < 7)) {
				//
				}else{
					pecarandomica = Random.Range (1, 5);
					if (pecarandomica == 3 || pecarandomica == 2) {
						Instantiate (sala, new Vector3 (x * 40, y, z * 40), Quaternion.Euler (0, 90.0f * Random.Range (1, 4), 0));
					} else {
						Instantiate (sala2, new Vector3 (x * 40, y, z * 40), Quaternion.Euler (0, 90.0f * Random.Range (1, 4), 0));
					}
				}
			}
		}

		for(x=1;x<10;x++){
			for (z = 1; z < 10; z++) {
				if ((x > 3 && x < 7 && z > 3 && z < 7)) {
				//
				}else{
					pecarandomica = Random.Range (1, 5);
					if (pecarandomica == 3 || pecarandomica == 2) {
						Instantiate (sala2, new Vector3 (x * 40, y + 15, z * 40), Quaternion.Euler (0, 90.0f * Random.Range (1, 4), 0));
					} else {
						Instantiate (sala, new Vector3 (x * 40, y + 15, z * 40), Quaternion.Euler (0, 90.0f * Random.Range (1, 4), 0));
					}
				}
			}
		}

		peca_padrao_giro = GameObject.FindGameObjectsWithTag("pecapadraogiro");
		jogador = GameObject.FindGameObjectWithTag("Player");
		gerador_poeira = GameObject.FindGameObjectsWithTag("criapoeira");
		criasoulsphere = GameObject.FindGameObjectsWithTag ("createsoulsphere");

		foreach (GameObject sphere in criasoulsphere) {
			if (Random.Range (1, 5) == 1) {
				Instantiate (soulsphere, new Vector3 (sphere.transform.position.x, sphere.transform.position.y, sphere.transform.position.z), Quaternion.identity);
			}
		}


	}

	// Update is called once per frame
	void Update () {

		correnteciclo += Time.deltaTime;

		if (correnteciclo > ciclo) {

			if (angulo_atual == 0) {
				Ambiente.GetComponent<AudioSource> ().PlayOneShot (Ambiente_sounds[0]);
			}

			move_pecas += Time.deltaTime; 

			if (move_pecas > 0.05f) {
				foreach (GameObject obj in peca_padrao_giro) {
					obj.transform.Rotate (0, 0.5f, 0, Space.World);
				}
				move_pecas = 0.0f;
				angulo_atual += 0.5f;
			}

			if (angulo_atual == 90.0f) {
				move_pecas = 0.0f;
				correnteciclo = 0.0f;
				angulo_atual = 0;
				Ambiente.GetComponent<AudioSource> ().PlayOneShot (Ambiente_sounds[1]);
				foreach (GameObject gera_poeira in gerador_poeira) {
					Instantiate (poeira, new Vector3(gera_poeira.transform.position.x,gera_poeira.transform.position.y,gera_poeira.transform.position.z),Quaternion.identity);
				}
			}
		}
	}
}
